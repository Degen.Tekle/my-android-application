package com.example.myapplication.ui.menu;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.example.myapplication.ui.welcome.WelcomeFragment;

public class Sensor4 extends Fragment implements SensorEventListener {
    private Button back;
    private Button quit;

    private SensorManager sensorManager;
    private Sensor magneticSensor;
    private TextView xValue, yValue, zValue, Comment;

    @SuppressLint("MissingInflatedId")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sensor4, container, false);

        xValue = rootView.findViewById(R.id.x2_value);
        yValue = rootView.findViewById(R.id.y2_value);
        zValue = rootView.findViewById(R.id.z2_value);
        Comment = rootView.findViewById(R.id.comment);
        back = rootView.findViewById(R.id.back4);
        back.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment1 fragment1 = new Fragment1();
                fragmentTransaction.add(R.id.fragment_container_view, fragment1);
                fragmentTransaction.commit();
            }
        });
        quit = rootView.findViewById(R.id.quit4);
        quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(0);
            }
        });

        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        magneticSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        sensorManager.registerListener(this, magneticSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            xValue.setText("X: " + x);
            yValue.setText("Y: " + y);
            zValue.setText("Z: " + z);
            Comment.setText(WelcomeFragment.name + ", voici les données du capteur magnétique.");

        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        
    }
}