package com.example.myapplication.ui.menu;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.R;
import com.example.myapplication.data.data0;
import com.example.myapplication.databinding.Fragment1Binding;
import com.example.myapplication.injection.ViewModelFactory;
import com.example.myapplication.ui.welcome.WelcomeFragment;

public class Fragment1 extends Fragment {
    private AppViewModel viewModel;

    Fragment1Binding binding;
    public static Fragment1 newInstance() {
        Fragment1 fragment = new Fragment1();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this, ViewModelFactory.getInstance()).get(AppViewModel.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = Fragment1Binding.inflate(inflater, container, false) ;
        return binding.getRoot() ;

    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.startMenu();
        viewModel.currentSentence.observe(getViewLifecycleOwner(), new Observer<data0>() {
            @Override
            public void onChanged(data0 data0) {
                updateSentence(data0);
            }
        });
        binding.sensor1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Sensor1 sensor1 = new Sensor1();
                fragmentTransaction.add(R.id.fragment_container_view, sensor1);
                fragmentTransaction.commit();
            }
        });
        binding.sensor2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Sensor2 sensor2 = new Sensor2();
                fragmentTransaction.add(R.id.fragment_container_view, sensor2);
                fragmentTransaction.commit();
            }
        });
        binding.sensor3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Sensor3 sensor3 = new Sensor3();
                fragmentTransaction.add(R.id.fragment_container_view, sensor3);
                fragmentTransaction.commit();
            }
        });
        binding.sensor4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Sensor4 sensor4 = new Sensor4();
                fragmentTransaction.add(R.id.fragment_container_view, sensor4);
                fragmentTransaction.commit();
            }
        });
        binding.back0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                WelcomeFragment welcomeFragment = new WelcomeFragment();
                fragmentTransaction.add(R.id.fragment_container_view, welcomeFragment);
                fragmentTransaction.commit();
            }
        });
        binding.quit0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(0);
            }
        });
    }

    private void updateSentence(data0 data0) {
        binding.title1.setText(data0.getSentence());
    }

}