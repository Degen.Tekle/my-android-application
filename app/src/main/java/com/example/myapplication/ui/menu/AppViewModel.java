package com.example.myapplication.ui.menu;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import com.example.myapplication.data.data0;
import com.example.myapplication.data.data1Repository;

import java.util.List;
import java.util.Random;

public class AppViewModel extends ViewModel {
    private data1Repository data1Repository;
    private List<data0> sentences;
    Random rand = new Random();
    private Integer currentSentenceIndex = rand.nextInt(4);

    public AppViewModel(com.example.myapplication.data.data1Repository data1Repository) {
        this.data1Repository = data1Repository;
    }

    MutableLiveData<data0> currentSentence = new MutableLiveData<data0>();

    public void startMenu(){
        sentences = data1Repository.getSentences();
        currentSentence.postValue(sentences.get(rand.nextInt(10)));
    }


}
