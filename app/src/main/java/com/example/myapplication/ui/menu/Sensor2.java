package com.example.myapplication.ui.menu;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.example.myapplication.ui.welcome.WelcomeFragment;

public class Sensor2 extends Fragment implements SensorEventListener {
    private Button back;
    private Button quit;
    private SensorManager sensorManager;
    private Sensor lightSensor;
    private TextView lightValue, Comment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Initialiser le SensorManager
        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);

        // Vérifier si le capteur de luminosité est disponible
        if (sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT) != null) {
            // Récupérer l'objet du capteur de luminosité
            lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        } else {
            // Le capteur de luminosité n'est pas disponible sur cet appareil
            // Gérer l'absence du capteur de luminosité
            throw new RuntimeException("Aucun capteur détecté.");
        }
    }

    @SuppressLint("MissingInflatedId")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sensor2, container, false);

        // Récupérer la référence du TextView
        lightValue = rootView.findViewById(R.id.light_value);
        Comment = rootView.findViewById(R.id.comment);
        back = rootView.findViewById(R.id.back2);
        back.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment1 fragment1 = new Fragment1();
                fragmentTransaction.add(R.id.fragment_container_view, fragment1);
                fragmentTransaction.commit();
            }
        });
        quit = rootView.findViewById(R.id.quit2);
        quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(0);
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Enregistrer le SensorEventListener pour le capteur de luminosité
        sensorManager.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Arrêter d'écouter les mises à jour du capteur lorsque le fragment est en pause
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // Vérifier le type de capteur
        if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
            // Récupérer la valeur de luminosité
            float light = event.values[0];

            // Afficher la valeur dans le TextView
            lightValue.setText("Light: " + light);
            Comment.setText(WelcomeFragment.name + ", voici les données du luxmètre.");

        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Gérer les changements de précision du capteur
    }
}