package com.example.myapplication.ui.menu;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.example.myapplication.ui.welcome.WelcomeFragment;

public class Sensor1 extends Fragment implements SensorEventListener {
    private Button back;
    private Button quit;
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private TextView xValue, yValue, zValue, Comment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Initialiser le SensorManager
        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);

        // Vérifier si l'accéléromètre est disponible
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            // Récupérer l'objet du capteur d'accéléromètre
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        } else {
            // L'accéléromètre n'est pas disponible sur cet appareil
            // Gérer l'absence du capteur d'accéléromètre
            throw new RuntimeException("Aucun capteur détecté.");
        }
    }

    @SuppressLint("MissingInflatedId")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sensor1, container, false);

        // Récupérer les références des TextViews
        xValue = rootView.findViewById(R.id.x_value);
        yValue = rootView.findViewById(R.id.y_value);
        zValue = rootView.findViewById(R.id.z_value);
        Comment = rootView.findViewById(R.id.comment);
        back = rootView.findViewById(R.id.back1);
        back.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment1 fragment1 = new Fragment1();
                fragmentTransaction.add(R.id.fragment_container_view, fragment1);
                fragmentTransaction.commit();
            }
        });
        quit = rootView.findViewById(R.id.quit1);
        quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(0);
            }
        });


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Enregistrer le SensorEventListener pour l'accéléromètre
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Arrêter d'écouter les mises à jour du capteur lorsque le fragment est en pause
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // Vérifier le type de capteur
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            // Récupérer les valeurs de l'accéléromètre
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            // Afficher les valeurs dans les TextViews
            xValue.setText("X: " + x);
            yValue.setText("Y: " + y);
            zValue.setText("Z: " + z);
            Comment.setText(WelcomeFragment.name + ", voici les données de l'accéléromètre.");
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Gérer les changements de précision du capteur
    }

}