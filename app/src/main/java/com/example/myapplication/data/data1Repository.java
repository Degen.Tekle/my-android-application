package com.example.myapplication.data;

import java.util.List;

public class data1Repository {
    private final data1 data1;

    public data1Repository(com.example.myapplication.data.data1 data1) {
        this.data1 = data1;
    }

    public List<data0> getSentences() {
        return data1.getSentences();
    }
}
