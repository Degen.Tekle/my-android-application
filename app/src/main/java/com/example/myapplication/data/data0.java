package com.example.myapplication.data;

import java.util.List;

public class data0 {
    private final String sentence;

    public data0(String sentence) {
        this.sentence = sentence;
    }

    public String getSentence() {
        return sentence;
    }
}
