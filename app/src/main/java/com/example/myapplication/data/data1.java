package com.example.myapplication.data;

import java.util.Arrays;
import java.util.List;

public class data1 {
    public List<data0> getSentences() {
        return Arrays.asList(
                new data0(
                        "De nombreux smartphones Android intègrent désormais des capteurs de fréquence cardiaque, permettant aux utilisateurs de mesurer leur pouls directement depuis leur téléphone."),
                new data0(
                        "Certains téléphones Android sont équipés de capteurs de pression atmosphérique, également connus sous le nom de baromètres, qui permettent de mesurer l'altitude et de fournir des informations météorologiques plus précises."),
                new data0(
                        "Bien que les smartphones Android ne disposent généralement pas d'un capteur de température dédié, certaines applications peuvent utiliser des informations telles que la température de la batterie ou la température ambiante pour fournir des indications approximatives sur la température."),
                new data0(
                        "Les smartphones Android sont équipés d'un accéléromètre qui permet de détecter les mouvements du téléphone."),
                new data0(
                        "Certains smartphones Android intègrent des capteurs de gaz, tels que le capteur de monoxyde de carbone (CO) ou de dioxyde d'azote (NO2), qui peuvent être utilisés pour mesurer la qualité de l'air."),
                new data0(
                        "Les smartphones Android sont équipés de capteurs de magnétomètre, également connus sous le nom de boussole électronique, qui détectent le champ magnétique terrestre."),
                new data0(
                        "Les smartphones Android intègrent des capteurs gyroscopiques qui mesurent l'orientation et les mouvements angulaires du téléphone."),
                new data0(
                        "Les smartphones Android sont équipés de capteurs de luminosité ambiante qui ajustent automatiquement la luminosité de l'écran en fonction des conditions d'éclairage environnantes."),
                new data0(
                        "Les smartphones Android sont dotés de capteurs de proximité qui détectent la présence de l'utilisateur à proximité de l'écran."),
                new data0(
                        "Les smartphones Android sont de plus en plus équipés de capteurs d'empreintes digitales intégrés dans l'écran ou à l'arrière du téléphone.")

        );
    }
    private static data1 instance;
    public static data1 getInstance() {
        if (instance == null) {
            instance = new data1();
        }
        return instance;
    }
}
