package com.example.myapplication.injection;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.myapplication.data.data1;
import com.example.myapplication.data.data1Repository;
import com.example.myapplication.ui.menu.AppViewModel;

import org.jetbrains.annotations.NotNull;

public class ViewModelFactory implements ViewModelProvider.Factory {
        private final data1Repository data1Repository;
        private static ViewModelFactory factory;

        public static ViewModelFactory getInstance() {
            if (factory == null) {
                synchronized (ViewModelFactory.class) {
                    if (factory == null) {
                        factory = new ViewModelFactory();
                    }
                }
            }
            return factory;
        }

        private ViewModelFactory() {
            data1 data1 = com.example.myapplication.data.data1.getInstance();
            this.data1Repository = new data1Repository(data1);
        }

        @Override
        @NotNull
        public <T extends ViewModel>  T create(Class<T> modelClass) {
            if (modelClass.isAssignableFrom(AppViewModel.class)) {
                return (T) new AppViewModel(data1Repository);
            }
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
}

